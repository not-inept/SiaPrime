Version History
---------------

Apr 2018:

v1.4.0 (minor release)
- Support "snapshot" backups
- Switch to new renter-host protocol
- Further scalability improvements

